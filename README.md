Скрипт для загрузки отзывов пользователей в группах ВК.

Для каждой группы ВК создается свой файл в формате JSON с отзывами (загружаются последние 100 отзывов)



1. Установка и настройка окружения (подразумевается, что python3 уже установлен в системе Linux):

    git clone https://kdza@bitbucket.org/kdza/vk_pump.git
    
    sudo apt install virtualenv
    
    cd vk_pump
    
    virtualenv venv -p python3
    
    source venv/bin/activate
    
    pip install vk_api

2. В файле config.py  указать  логин и пароль к сети ВКонтакте

3. В файле input_urls.txt указать ссылки на группы ВК, каждая ссылка - с новой строки (можно назначить другой файл через параметр 'input_filename'  в файле config.py)

4. Скрипт запускать командой:
    
    python run.py
    
5. Результат смотреть в папке out   (можно назначить другую папку через параметр 'output_dir'  в файле config.py)

6. Лог созраняется в файл log.txt

