# -*- coding: utf-8 -*-
import vk_api
import logging
import json
import os
import config  # наш конфиг


logging.basicConfig(filename="log.txt", format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger('VK_pump')


def save_json(obj, path):
    u"""сохранение объекта"""
    with open(path, mode='w') as f:
        str1 = json.dumps(obj, indent=2, ensure_ascii=False)
        f.write(str1)


def load_json(path):
    u"""загрузка объекта"""
    with open(path, "r") as f:
        return json.load(f)


def get_board_comments(vk, url):
    """ загружает последние 100 комментариев-отзывов из группы ВК """
    log.info('----------------------------------------')
    log.info('обрабатываю url: %s' % url)
    if url.count('vk.com/') <= 0:
        log.error('Ожидаю ссылку на группу ВК')
        return

    screen_name = url.split('vk.com/')[1]

    # Определим тип объекта и его идентификатор по короткому имени screen_name.
    obj = vk.utils.resolveScreenName(screen_name=screen_name)
    if obj:
        if 'group' == obj['type']:
            group_id = obj['object_id']
        else:
            log.error('Нужно указать ссылку на группу ВК')
            return
    else:
        log.error('Несуществующая страница ВК')
        return

    # Получаем список тем обсуждений, чтобы найти Отзывы
    resp = vk.board.getTopics(group_id=group_id)
    if not resp['items']:
        log.info('Не найдено ни одного обсуждения')
        return

    topic_id = None
    for theme in resp['items']:
        if theme['title'].upper() == 'ОТЗЫВЫ':
            topic_id = theme['id']
            log.info('всего отзывов: %i' % theme['comments'])
            break

    if not topic_id:
        log.info('Не обнаружено обсуждение ОТЗЫВЫ')
        return

    # Получаем список комменариев пользователей в теме ОТЗЫВЫ
    resp = vk.board.getComments(group_id=group_id, topic_id=topic_id, count=100, extended=1, sort='desc')
    log.info('загружено отзывов: %i' % len(resp['items']))

    # сделаем словарь юзеров с ключем = id юзера
    users = dict((k['id'], k) for k in resp['profiles'])
    for r in resp['items']:
        user_id = r['from_id']
        r['from_user'] = users[user_id]
        del r['from_id']

    return {'url': url, 'screen_name': screen_name, 'items': resp['items']}


def main():
    """ получение списка коменнтариев пользователей в теме обсуждения ОТЗЫВЫ """
    log.info('==========================================================================')
    log.info('Старт')
    input_filename = config.input_filename

    # чтение ссылок из текстового файла, где каждый url - с новой строки
    try:
        with open(input_filename) as f:
            urls = f.read().splitlines()
    except FileNotFoundError:
        log.error('Не найден входной файл со ссылками на группы ВК : %s' % input_filename)
        return

    login, password = config.vk_login, config.vk_password
    vk_session = vk_api.VkApi(login, password)

    try:
        vk_session.auth(token_only=True)
    except vk_api.AuthError as error_msg:
        log.error(error_msg)
        return

    vk = vk_session.get_api()

    # создадим папку на диске
    output_dir = config.output_dir
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # обработка ссылок
    for url in urls:
        res = get_board_comments(vk, url)
        if res:
            save_json(res, output_dir+'/'+res['screen_name']+'.json')

    log.info('Финиш')


if __name__ == '__main__':
    main()
